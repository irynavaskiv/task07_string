package com.vaskiv.Internationalization;

import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by user on 13.02.2019.
 */
public class i18n {

    public static void main (String[]args) throws SQLException, InterruptedException{
        ResourceBundle bundleDefault = ResourceBundle.getBundle("resources.properties");
        ResourceBundle bundleEn = ResourceBundle.getBundle("resources", new Locale("en","USA"));
        ResourceBundle bundleDe = ResourceBundle.getBundle("resources", new Locale("de","GR"));
        ResourceBundle bundleFr = ResourceBundle.getBundle("resources", new Locale("fr","FR"));

        System.out.println(bundleDefault.getString("somevalue"));
        System.out.println(bundleEn.getString("somevalue"));
        System.out.println(bundleDe.getString("somevalue"));
        System.out.println(bundleFr.getString("somevalue"));

    }

}

